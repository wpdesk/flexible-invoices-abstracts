## [1.3.2] - 2023-06-20
### Added
- library update
## [1.3.1] - 2023-06-20
### Added
- library update
## [1.3.0] - 2022-03-18
### Removed
- remove additional_data unused additional_data methods
### Added
- state field

## [1.2.0] - 2021-08-05
### Fixed
- project namespace

## [1.1.0] - 2021-07-25
### Fixed
- qty fix

## [1.0.2] - 2021-01-25
### Added
- street2 field for customer

## [1.0.1] - 2020-11-02
### Fixed
- rename methods

## [1.0.0] - 2020-10-18
### Added
- init
