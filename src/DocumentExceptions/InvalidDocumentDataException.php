<?php

namespace WPDesk\Library\FlexibleInvoicesAbstracts\DocumentExceptions;

/**
 * Throw exception when the data of the document will not be correct.
 *
 * @package WPDesk\Library\FlexibleInvoicesAbstracts\Exceptions
 */
class InvalidDocumentDataException extends \RuntimeException implements DocumentException {

}
