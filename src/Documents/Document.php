<?php

namespace WPDesk\Library\FlexibleInvoicesAbstracts\Documents;

/**
 * Define Document.
 */
interface Document extends DocumentGetters, DocumentSetters {

}
