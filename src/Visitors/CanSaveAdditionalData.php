<?php

namespace WPDesk\Library\FlexibleInvoicesAbstracts\Visitors;

use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;

/**
 * Interface for declaring the classes that will be responsible for saving additional data.
 *
 */
interface CanSaveAdditionalData {

    /**
     * @param int      $document_id
     * @param Document $document
     *
     * @return void
     */
    public function save_document( $document_id, Document $document );

}
