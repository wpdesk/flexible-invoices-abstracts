<?php

namespace WPDesk\Library\FlexibleInvoicesAbstracts\DocumentData;

/**
 * Items data.
 */
interface Items {

    /**
     * @return array
     */
    public function get_items();

}
